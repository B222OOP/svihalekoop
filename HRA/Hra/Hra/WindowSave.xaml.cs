﻿using CV03;
using Microsoft.VisualBasic;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static System.Formats.Asn1.AsnWriter;

namespace Hra
{
    /// <summary>
    /// Interaction logic for WindowSave.xaml
    /// </summary>
    public partial class WindowSave : Window
    {
        private MainWindow mainWindow;
        private DB_connect db_conn;
        private MySqlDataReader reader;
        private string name;
        private long score;
        public WindowSave(MainWindow mainWindow, DB_connect db_conn, MySqlDataReader reader)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.db_conn = db_conn;
            this.reader = reader;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            bool atLeastOne = false;
            if (tbName.Text.Equals(""))
            {
                MessageBox.Show("Fill in your name");
                return;
            }
            reader = db_conn.Select($"SELECT `Name`, `Score` FROM `Game_Top_Users` WHERE Name=\"{tbName.Text}\"");
            while (reader.Read())
            {
                atLeastOne = true;
                name = reader.GetString(0);
                score = reader.GetInt64(1);
            }
            if (atLeastOne)
            {
                if (name.Equals(tbName.Text) && score < mainWindow.score)
                {
                    db_conn.Update($"UPDATE `Game_Top_Users` SET `Score`={mainWindow.score} WHERE Name=\"{name}\"");
                    this.Close();
                    return;
                }
                else if (name.Equals(tbName.Text))
                {
                    MessageBox.Show("This name has higher score recorded.");
                    return;
                }
            }
            db_conn.Insert($"INSERT INTO `Game_Top_Users`(`Name`, `Score`) VALUES (\"{tbName.Text}\",{mainWindow.score})");
            MessageBox.Show("Success");
            this.Close();
        }
    }
}
