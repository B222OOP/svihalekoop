﻿using CV03;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hra
{
    /// <summary>
    /// Interaction logic for WindowBest.xaml
    /// </summary>
    ///
    public partial class WindowBest : Window
    {
        private DB_connect db_conn;
        private MySqlDataReader reader;
        private List<NameScore> nameScores = new List<NameScore>();
        public WindowBest(DB_connect db_conn, MySqlDataReader reader)
        {
            InitializeComponent();
            this.db_conn = db_conn;
            this.reader = reader;

            reader = db_conn.Select($"SELECT * FROM `Game_Top_Users` WHERE 1 ORDER BY Score DESC");
            while (reader.Read())
            {
                NameScore nameScore = new NameScore(reader.GetString(0), reader.GetInt64(1));
                nameScores.Add(nameScore);
            }

            lbNames.ItemsSource = null;
            lbNames.ItemsSource = nameScores;
        }
    }
}
