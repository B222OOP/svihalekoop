﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hra
{
    internal class NameScore
    {
        public string name;
        public long score;

        public NameScore(string name, long score)
        {
            this.name = name;
            this.score = score;
        }

        public override string ToString()
        {
            return name + ": " + score;
        }
    }
}
