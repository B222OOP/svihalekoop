﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hra
{
    internal class Platform
    {
        private Rectangle platform = new Rectangle();
        private Rectangle platformColumn = new Rectangle();
        private Point position = new Point();

        public Platform(Canvas cnv)
        {
            // create horizontal part
            platform.Fill = new SolidColorBrush(Colors.Blue);
            platform.Width = 300;
            platform.Height = 10;
            position.X = 200;
            position.Y = 300;
            Canvas.SetLeft(platform, position.X);
            Canvas.SetTop(platform, position.Y);
            cnv.Children.Add(platform);

            // create vertical part
            platformColumn.Fill = new SolidColorBrush(Colors.Blue);
            platformColumn.Width = 10;
            platformColumn.Height = 40;
            position.X = 345;
            position.Y = 310;
            Canvas.SetLeft(platformColumn, position.X);
            Canvas.SetTop(platformColumn, position.Y);
            cnv.Children.Add(platformColumn);

        }
    }
}
