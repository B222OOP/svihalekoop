﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.CodeDom.Compiler;
using CV03;
using MySqlConnector;

namespace Hra
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer dispatcherTimerMove = new DispatcherTimer();
        private DispatcherTimer dispatcherTimerJump = new DispatcherTimer();
        private Player player;
        private Platform platform;
        private bool firstTime = true;
        public long score = 0;
        private bool canStartAnotherGame = false;
        private DB_connect dbconn = new DB_connect();
        private MySqlDataReader reader;
        private bool didWeSave = false;


        public MainWindow()
        {
            InitializeComponent();
            cnv.Focus();
            btnStart.Focusable = false;

            platform = new Platform(cnv);
            player = new Player(this, cnv);

            dispatcherTimerMove.Tick += new EventHandler(dispatcherTimerMove_Tick);
            dispatcherTimerJump.Tick += new EventHandler(dispatcherTimerJump_Tick);
            dispatcherTimerMove.Interval = new TimeSpan(0, 0, 0, 0, 1);
            dispatcherTimerJump.Interval = new TimeSpan(0, 0, 0, 0, 1);

            lbScore.Content = score;
        }

        private void dispatcherTimerMove_Tick(object sender, EventArgs e)
        {
            player.move();
        }

        private void dispatcherTimerJump_Tick(object sender, EventArgs e)
        {
            player.jump(dispatcherTimerJump);
        }

        private void cnv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.IsRepeat || player.gameEnded || firstTime)
            {
                return;
            }
            switch (e.Key)
            {
                case Key.Down:
                    player.makeSmall();
                    player.isSmallForm = true;
                    return;
                case Key.Up:
                    if (player.isJumping)
                    {
                        return;
                    }
                    player.isJumping = true;
                    player.jump(dispatcherTimerJump);
                    dispatcherTimerJump.Start();
                    return;
                case Key.Right:
                    player.movingRight = true;
                    break;
                case Key.Left:
                    player.movingLeft = true;
                    break;
            }

            if (player.movingLeft && player.movingRight)
            {
                dispatcherTimerMove.Stop();
                return;
            }
            player.move();
            dispatcherTimerMove.Start();
        }

        private void cnv_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.IsRepeat || player.gameEnded || firstTime)
            {
                return;
            }
            switch (e.Key)
            {
                case Key.Down:
                    if (!player.isSmallForm)
                    {
                        return;
                    }
                    player.makeNormal();
                    player.isSmallForm = false;
                    return;
                case Key.Up:
                    player.isJumping = false;
                    return;
                case Key.Right:
                    player.movingRight = false;
                    break;
                case Key.Left:
                    player.movingLeft = false;
                    break;
            }
            if (player.movingLeft || player.movingRight)
            {
                player.move();
                dispatcherTimerMove.Start();
                return;
            }
            dispatcherTimerMove.Stop();
        }

        private async void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (!firstTime && !player.gameEnded)
            {
                return;
            }
            else if (player.gameEnded && !canStartAnotherGame)
            {
                return;
            }
            else if (player.gameEnded)
            {
                lbGameOver.Visibility = Visibility.Hidden;
                score = 0;
                lbScore.Content = score;
                btnSave.Visibility = Visibility.Hidden;
                player.getPlayerToDefaultPosition();
                player.gameEnded = false;
                canStartAnotherGame = false;
                didWeSave = false;
            }
            firstTime = false;

            int delay = 2000;
            Random rnd = new Random();
            bool[] positions = { false, false };
            bool isUp = false;

            while (true)
            {
                if (player.gameEnded)
                {
                    canStartAnotherGame = true;
                    break;
                }
                if (rnd.Next(0,2) == 0)
                {
                    positions = GenerateBulletPosition(rnd);
                    isUp = false;
                }
                else
                {
                    isUp = true;
                }
                Bullet bullet = new Bullet(this, cnv, player, positions[0], positions[1], isUp);
                bullet.dispatcherTimerBullet.Start();
                await Task.Delay(delay);
                if (delay > 500 && delay <= 1000)
                {
                    delay -= 50;
                }
                else if (delay > 1000)
                {
                    delay -= 100;
                }
            }
        }

        public void EndGame()
        {
            dispatcherTimerJump.Stop();
            dispatcherTimerMove.Stop();
            lbGameOver.Visibility = Visibility.Visible;
            btnSave.Visibility= Visibility.Visible;
        }

        private bool[] GenerateBulletPosition(Random rnd)
        {
            int firstNum = rnd.Next(0, 2);
            int secondNum = rnd.Next(0, 2);
            if (firstNum == 0 && secondNum == 0)
            {
                return new bool[] { false, false };
            }
            else if (firstNum == 0)
            {
                return new bool[] { false, true };
            }
            else if (secondNum == 0)
            {
                return new bool[] { true, false };
            }
            else
            {
                return new bool[] { true, true };
            }
        }

        public long addScore()
        {
            score += 10;
            return score;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!player.gameEnded || didWeSave)
            {
                return;
            }
            WindowSave windowSave = new WindowSave(this, dbconn, reader);
            windowSave.Show();

            didWeSave = true;
        }

        private void btnBest_Click(object sender, RoutedEventArgs e)
        {
            if (!firstTime && !player.gameEnded)
            {
                return;
            }
            WindowBest windowBest = new WindowBest(dbconn, reader);
            windowBest.Show();
        }
    }
}
