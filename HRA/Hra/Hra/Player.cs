﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Hra
{
    internal class Player
    {
        public Rectangle player = new Rectangle();
        public Point position = new Point();
        public bool movingRight = false;
        public bool movingLeft = false;
        public bool isJumping = false;
        public bool isSmallForm = false;
        private bool isFalling = false;
        public bool gameEnded = false;
        private int jumpValue = -2;
        private bool maxHeightReached = false;
        private DispatcherTimer dispatcherTimerFall = new DispatcherTimer();
        private Canvas cnv;
        private MainWindow mainWindow;

        public Player(MainWindow mainWindow ,Canvas cnv)
        {
            this.cnv = cnv;
            this.mainWindow = mainWindow;
            player.Fill = new SolidColorBrush(Colors.Green);
            player.Width = 10;
            player.Height = 20;
            position.X = 345;
            position.Y = 280;
            Canvas.SetLeft(player, position.X);
            Canvas.SetTop(player, position.Y);
            cnv.Children.Add(player);
            dispatcherTimerFall.Tick += new EventHandler(dispatcherTimerFall_Tick);
            dispatcherTimerFall.Interval = new TimeSpan(0, 0, 0, 0, 1);
        }
        private void dispatcherTimerFall_Tick(object sender, EventArgs e)
        {
            Fall();
        }

        public void move()
        {
            if (isFalling)
            {
                return;
            }
            if (movingLeft)
            {
                moveLeft();
            }
            if (movingRight)
            {
                moveRight();
            }
        }

        public void jump(DispatcherTimer dispatcherTimerJump)
        {
            if (isFalling)
            {
                return;
            }
            if (isSmallForm)
            {
                if (!maxHeightReached && position.Y <= 280)
                {
                    jumpValue = 2;
                    maxHeightReached = true;
                }
                else if (maxHeightReached && position.Y == 290)
                {
                    jumpValue = -2;
                    maxHeightReached = false;
                    isJumping = false;
                    dispatcherTimerJump.Stop();
                    return;
                }
                position.Y += jumpValue;
                Canvas.SetTop(player, position.Y);
            }
            else
            {
                if (!maxHeightReached && position.Y <= 260)
                {
                    jumpValue = 2;
                    maxHeightReached = true;
                }
                else if (maxHeightReached && position.Y == 280)
                {
                    jumpValue = -2;
                    maxHeightReached = false;
                    isJumping = false;
                    dispatcherTimerJump.Stop();
                    return;
                }
                position.Y += jumpValue;
                Canvas.SetTop(player, position.Y);
            }
        }

        public void makeSmall()
        {
            if (isSmallForm || isFalling)
            {
                return;
            }
            isSmallForm = true;
            player.Width = 20;
            player.Height = 10;
            position.X -= 5;
            position.Y += 10;
            Canvas.SetLeft(player, position.X);
            Canvas.SetTop(player, position.Y);
        }

        public void makeNormal()
        {
            if (!isSmallForm || isFalling)
            {
                return;
            }
            isSmallForm = false;
            player.Width = 10;
            player.Height = 20;
            position.X += 5;
            position.Y -= 10;
            Canvas.SetLeft(player, position.X);
            Canvas.SetTop(player, position.Y);
        }

        private void moveLeft()
        {
            if (isSmallForm)
            {
                position.X -= 2;
            }
            else
            {
                position.X -= 5;
            }
            Canvas.SetLeft(player, position.X);

            if (!isFalling && ((isSmallForm && position.X <= 180) || (!isSmallForm && position.X <= 190)))
            {
                isFalling = true;
                dispatcherTimerFall.Start();
            }
        }
        private void moveRight()
        {
            if (isSmallForm)
            {
                position.X += 2;
            }
            else
            {
                position.X += 5;
            }
            Canvas.SetLeft(player, position.X);

            if (!isFalling && position.X >= 500)
            {
                isFalling = true;
                dispatcherTimerFall.Start();
            }
        }

        private void Fall()
        {
            if ((isSmallForm && position.Y >= 340) || position.Y >= 330)
            {
                dispatcherTimerFall.Stop();
                EndGame();
                mainWindow.EndGame();
                return;
            }
            position.Y += 5;
            Canvas.SetTop(player, position.Y);
            return;
        }

        public void EndGame()
        {
            gameEnded = true;
        }

        public void getPlayerToDefaultPosition()
        {
            player.Width = 10;
            player.Height = 20;
            position.X = 345;
            position.Y = 280;
            Canvas.SetLeft(player, position.X);
            Canvas.SetTop(player, position.Y);
            movingRight = false;
            movingLeft = false;
            isJumping = false;
            isSmallForm = false;
            isFalling = false;
            gameEnded = false;
            jumpValue = -2;
            maxHeightReached = false;
    }
    }
}
