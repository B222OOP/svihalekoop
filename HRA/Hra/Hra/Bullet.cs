﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Hra
{

    internal class Bullet
    {
        private Rectangle rct = new Rectangle();
        private Point position = new Point();
        public DispatcherTimer dispatcherTimerBullet = new DispatcherTimer();
        public bool isTop;
        public bool isLeft;
        public bool isUp;
        public bool stoppedOnMe = false;
        private Canvas cnv;
        private Player player;
        private MainWindow mainWindow;
        private Random rnd = new Random();

        public Bullet(MainWindow mainWindow, Canvas cnv, Player player, bool isTop, bool isLeft, bool isUp)
        {
            dispatcherTimerBullet.Tick += new EventHandler(dispatcherTimerBullet_Tick);
            dispatcherTimerBullet.Interval = new TimeSpan(0, 0, 0, 0, 1);
            rct.Fill = new SolidColorBrush(Colors.Red);
            rct.Height = 10;
            rct.Width = 10;
            this.mainWindow = mainWindow;
            this.cnv = cnv;
            this.player = player;
            this.isTop = isTop;
            this.isLeft = isLeft;
            this.isUp = isUp;
            if (isUp)
            {
                GeneratePositionOnTop();
            }
            else
            {
                if (isTop)
                {
                    position.Y = 290;
                }
                else
                {
                    position.Y = 280;
                }
                if (isLeft)
                {
                    position.X = 0;
                }
                else
                {
                    position.X = 700;
                }
            }
            Canvas.SetLeft(rct, position.X);
            Canvas.SetTop(rct, position.Y);
            cnv.Children.Add(rct);
        }

        private void GeneratePositionOnTop()
        {
            int leftEdge = (int)player.position.X - 25;
            int rightEdge = (int)player.position.X + 26;

            position.X = rnd.Next(leftEdge, rightEdge);
            position.Y = 0;
        }

        private void dispatcherTimerBullet_Tick(object sender, EventArgs e)
        {
            if (player.gameEnded)
            {
                if (stoppedOnMe)
                {
                    mainWindow.EndGame();
                }
                dispatcherTimerBullet.Stop();
                rct.Visibility = Visibility.Collapsed;
                return;
            }
            if (isUp)
            {
                moveDown();
                return;
            }
            else if (isLeft)
            {
                moveLeft();
                return;
            }
            moveRight();
        }
        public void moveDown()
        {
            if (position.Y >= 290)
            {
                dispatcherTimerBullet.Stop();
                rct.Visibility = Visibility.Collapsed;
                mainWindow.lbScore.Content = mainWindow.addScore();
                return;
            }
            position.Y += 5;
            Canvas.SetTop(rct, position.Y);

            if (Hit())
            {
                player.EndGame();
                stoppedOnMe = true;
            }
        }

        public void moveLeft()
        {
            if (position.X >= 690)
            {
                dispatcherTimerBullet.Stop();
                rct.Visibility = Visibility.Collapsed;
                mainWindow.lbScore.Content = mainWindow.addScore();
                return;
            }
            position.X += 5;
            Canvas.SetLeft(rct, position.X);

            if (Hit())
            {
                player.EndGame();
                stoppedOnMe = true;
            }
        }

        public void moveRight()
        {
            if (position.X <= 0)
            {
                dispatcherTimerBullet.Stop();
                rct.Visibility = Visibility.Collapsed;
                mainWindow.lbScore.Content = mainWindow.addScore();
                return;
            }
            position.X -= 5;
            Canvas.SetLeft(rct, position.X);

            if (Hit())
            {
                player.EndGame();
                stoppedOnMe = true;
            }
        }

        private bool Hit()
        {
            if (player.isSmallForm)
            {
                if (((player.position.X > position.X && player.position.X < position.X + 10) || (player.position.X + 20 > position.X && player.position.X < position.X + 10)) &&
                    ((player.position.Y > position.Y && player.position.Y < position.Y + 10) || (player.position.Y + 10 > position.Y && player.position.Y < position.Y + 10)))
                {
                    return true;
                }
            }
            else
            {
                if (((player.position.X > position.X && player.position.X < position.X + 10) || (player.position.X + 10 > position.X && player.position.X < position.X + 10)) &&
                    ((player.position.Y > position.Y && player.position.Y < position.Y + 10) || (player.position.Y + 20 > position.Y && player.position.Y < position.Y + 10)))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
