﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowAddZakaznik.xaml
    /// </summary>
    public partial class WindowAddZakaznik : Window
    {
        public MainWindow mainWindow;
        public WindowAddZakaznik(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
        }

        private void btnPridej_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.dbconn.Insert($"INSERT INTO `T_Uzivatel`(`Jmeno`, `Prijmeni`, `Prezdivka`, `Heslo`, `Role`) VALUES (\"{tbJmeno.Text}\", \"{tbPrijmeni.Text}\", \"{tbPrezdivka.Text}\", \"{tbHeslo.Text}\", {Int32.Parse(tbRole.Text)})");
            

            if (Int32.Parse(tbRole.Text) == 2)
            {
                mainWindow.reader = mainWindow.dbconn.Select($"SELECT MAX(Id) FROM T_Uzivatel WHERE 1");
                while (mainWindow.reader.Read())
                {
                    Uzivatel uzivatel = new Uzivatel(tbJmeno.Text, tbPrijmeni.Text, tbPrezdivka.Text, tbHeslo.Text, Int32.Parse(tbRole.Text), mainWindow.reader.GetInt32(0));
                    mainWindow.uzivatele.Add(uzivatel);
                }
            }
            this.Close();
        }
    }
}
