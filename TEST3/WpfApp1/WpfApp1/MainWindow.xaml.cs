﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySqlConnector;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DB_connect dbconn;
        public MySqlDataReader reader;
        public List<Film> filmy = new List<Film>();
        public List<Uzivatel> uzivatele = new List<Uzivatel>();
        public List<Rezervace> rezervace = new List<Rezervace>();
        public Uzivatel uzivatel;

        public MainWindow()
        {
            InitializeComponent();
            dbconn = new DB_connect();
            reader = dbconn.Select($"SELECT * FROM T_Film WHERE 1");
            while (reader.Read())
            {
                Film film = new Film(reader.GetString(0), reader.GetString(1), reader.GetBoolean(2), reader.GetInt32(3));
                filmy.Add(film);
            }

            reader = dbconn.Select($"SELECT * FROM T_Uzivatel WHERE Role=2");
            while (reader.Read())
            {
                Uzivatel uzivatel = new Uzivatel(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetInt32(4), reader.GetInt32(5));
                uzivatele.Add(uzivatel);
            }
            reader = dbconn.Select($"SELECT * FROM T_Rezervace WHERE 1");
            while (reader.Read())
            {
                Rezervace rezervaceJedna = new Rezervace(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2), reader.GetInt32(3));
                rezervace.Add(rezervaceJedna);
            }
        }

        private void btnPrihlasit_Click(object sender, RoutedEventArgs e)
        {
            string prezdivka = txtJmeno.Text;
            string heslo = txtHeslo.Text;

            reader = dbconn.Select($"SELECT * FROM T_Uzivatel WHERE Prezdivka=\"{prezdivka}\" AND Heslo=\"{heslo}\"");
            while (reader.Read())
            {
                uzivatel = new Uzivatel(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetInt32(4), reader.GetInt32(5));
                if (reader.GetInt32(4) == 1)
                {
                    WindowAdmin windowAdmin = new WindowAdmin(this);
                    windowAdmin.ShowDialog();
                }
                else
                {
                    WindowUser windowUser = new WindowUser(this);
                    windowUser.ShowDialog();
                }
            }
        }
    }
}
