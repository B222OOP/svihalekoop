﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySqlConnector;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowAdmin.xaml
    /// </summary>
    public partial class WindowAdmin : Window
    {
        MainWindow mainWindow;

        public WindowAdmin(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;

            lbFilmy.ItemsSource = mainWindow.filmy;
            lbZakaznici.ItemsSource = mainWindow.uzivatele;
            lbRezervace.ItemsSource = mainWindow.rezervace;
        }

        private void lbFilmy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Film film = (Film)lbFilmy.SelectedItem;
            if (film != null)
            {
                tbInfo.Text = film.getInfo();
            }
        }

        private void lbZakaznici_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Uzivatel uzivatel = (Uzivatel)lbZakaznici.SelectedItem;
            if (uzivatel != null)
            {
                tbZakaznik.Text = uzivatel.getInfo();
            }
        }

        private void btnPridej_Click(object sender, RoutedEventArgs e)
        {
            WindowAddZakaznik windowAddZakaznik = new WindowAddZakaznik(mainWindow);
            windowAddZakaznik.ShowDialog();
            resetLBZakaznici();
        }

        private void btnSmaz_Click(object sender, RoutedEventArgs e)
        {
            if (lbZakaznici.SelectedItem != null)
            {
                Uzivatel uzivatel = (Uzivatel)lbZakaznici.SelectedItem;
                mainWindow.dbconn.Delete($"DELETE FROM `T_Uzivatel` WHERE Id={uzivatel.id}");
                mainWindow.uzivatele.Remove(uzivatel);
            }
            resetLBZakaznici();
            tbZakaznik.Text = "";
        }

        public void resetLBZakaznici()
        {
            lbZakaznici.ItemsSource = null;
            lbZakaznici.ItemsSource = mainWindow.uzivatele;
        }
        public void resetLBRezervace()
        {
            lbRezervace.ItemsSource = null;
            lbRezervace.ItemsSource = mainWindow.rezervace;
        }

        public void resetLBFilmu()
        {
            lbFilmy.ItemsSource = null;
            lbFilmy.ItemsSource = mainWindow.filmy;
        }

        private void lbRezervace_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Rezervace rezervace = (Rezervace)lbRezervace.SelectedItem;
            if (rezervace != null)
            {
                tbRezervace.Text = rezervace.getInfo();
            }
        }
        private void btnPridejR_Click(object sender, RoutedEventArgs e)
        {
            WindowAddRezervaci windowAddRezervaci = new WindowAddRezervaci(mainWindow);
            windowAddRezervaci.ShowDialog();
            resetLBRezervace();
            resetLBFilmu();
            resetLBZakaznici();
            Film film = (Film)lbFilmy.SelectedItem;
            if (film != null)
            {
                tbInfo.Text = film.getInfo();
            }
        }

        private void btnSmazR_Click(object sender, RoutedEventArgs e)
        {
            if (lbRezervace.SelectedItem != null)
            {
                Rezervace rezervaceJedna = (Rezervace)lbRezervace.SelectedItem;
                mainWindow.dbconn.Delete($"DELETE FROM `T_Rezervace` WHERE Id={rezervaceJedna.id}");
                mainWindow.rezervace.Remove(rezervaceJedna);

                mainWindow.dbconn.Update($"UPDATE `T_Film` SET `Je_Rezervovan`= 0 WHERE Id={rezervaceJedna.film}");
                foreach (Film film in mainWindow.filmy)
                {
                    if (film.id == rezervaceJedna.film)
                    {
                        film.je_rezervovan = false;
                    }
                }
            }
            resetLBRezervace();
            resetLBFilmu();
            resetLBZakaznici();
        }

        private void btnFJmeno_Click(object sender, RoutedEventArgs e)
        {
            List<Uzivatel> uzivatele = mainWindow.uzivatele;
            uzivatele.Sort(sortJmeno);
            lbZakaznici.ItemsSource = null;
            lbZakaznici.ItemsSource = uzivatele;
        }

        public int sortJmeno(Uzivatel a, Uzivatel b)
        {
            return a.jmeno.CompareTo(b.jmeno);
        }
        public int sortPrijmeni(Uzivatel a, Uzivatel b)
        {
            return a.prijmeni.CompareTo(b.prijmeni);
        }

        public int sortPrezdivka(Uzivatel a, Uzivatel b)
        {
            return a.prezdivka.CompareTo(b.prezdivka);
        }
        public int sortNazev(Film a, Film b)
        {
            return a.nazev.CompareTo(b.nazev);
        }

        public int sortZanr(Film a, Film b)
        {
            return a.zanr.CompareTo(b.zanr);
        }

        public int sortDatum(Film a, Film b)
        {
            if (a.je_rezervovan.CompareTo(b.je_rezervovan) == 0 && a.je_rezervovan == true)
            {
                string datumA = "";
                string datumB = "";
                foreach (Rezervace rezervace in mainWindow.rezervace)
                {
                    if (rezervace.film == a.id)
                    {
                        datumA = rezervace.datum_platnosti;
                    }
                    if (rezervace.film == b.id)
                    {
                        datumB = rezervace.datum_platnosti;
                    }
                }
                string[] pole = datumA.Split(".");
                string[] pole1 = datumB.Split(".");
                DateTime A = new DateTime(Int32.Parse(pole[2]), Int32.Parse(pole[1]), Int32.Parse(pole[0]));
                DateTime B = new DateTime(Int32.Parse(pole1[2]), Int32.Parse(pole1[1]), Int32.Parse(pole1[0]));
                return DateTime.Compare(A, B);
            }
            else
            {
                return a.je_rezervovan.CompareTo(b.je_rezervovan);
            }
        }

        private void btnFPrijmeni_Click(object sender, RoutedEventArgs e)
        {
            List<Uzivatel> uzivatele = mainWindow.uzivatele;
            uzivatele.Sort(sortPrijmeni);
            lbZakaznici.ItemsSource = null;
            lbZakaznici.ItemsSource = uzivatele;
        }

        private void btnFPrezdivka_Click(object sender, RoutedEventArgs e)
        {
            List<Uzivatel> uzivatele = mainWindow.uzivatele;
            uzivatele.Sort(sortPrezdivka);
            lbZakaznici.ItemsSource = null;
            lbZakaznici.ItemsSource = uzivatele;
        }

        private void btnFNazev_Click(object sender, RoutedEventArgs e)
        {
            List<Film> filmy = mainWindow.filmy;
            filmy.Sort(sortNazev);
            lbFilmy.ItemsSource = null;
            lbFilmy.ItemsSource = filmy;
        }

        private void btnFZanr_Click(object sender, RoutedEventArgs e)
        {
            List<Film> filmy = mainWindow.filmy;
            filmy.Sort(sortZanr);
            lbFilmy.ItemsSource = null;
            lbFilmy.ItemsSource = filmy;
        }

        private void btnFDatum_Click(object sender, RoutedEventArgs e)
        {
            List<Film> filmy = mainWindow.filmy;
            filmy.Sort(sortDatum);
            lbFilmy.ItemsSource = null;
            lbFilmy.ItemsSource = filmy;
        }
    }
}
