﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1
{
    public class Uzivatel
    {
        public string jmeno;
        public string prijmeni;
        public string prezdivka;
        public string heslo;
        public int role_id;
        public int id;

        public Uzivatel(string jmeno, string prijmeni, string prezdivka, string heslo, int role_id, int id)
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.prezdivka = prezdivka;
            this.heslo = heslo;
            this.role_id = role_id;
            this.id = id;
        }

        public override string ToString()
        {
            return $"{jmeno} {prijmeni} ({prezdivka})";
        }

        public string getInfo()
        {
            return $"jmeno: {jmeno} \n prijmeni: {prijmeni} \n prezdivka: {prezdivka} \n heslo: {heslo} \n role_id: {role_id} \n id: {id}";
        }
    }
}
