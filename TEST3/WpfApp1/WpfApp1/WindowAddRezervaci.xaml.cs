﻿using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowAddRezervaci.xaml
    /// </summary>
    public partial class WindowAddRezervaci : Window
    {

        MainWindow mainWindow;

        public WindowAddRezervaci(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
        }

        private void btnPridej_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.reader = mainWindow.dbconn.Select($"SELECT * FROM T_Film WHERE Id={tbFilm.Text}");
            while (mainWindow.reader.Read())
            {
                if (mainWindow.reader.GetBoolean(2) == true)
                {
                    MessageBox.Show("Film je uz nekym rezervovan");
                    return;
                }
                else
                {
                    mainWindow.dbconn.Update($"UPDATE `T_Film` SET `Je_Rezervovan`= 1 WHERE Id={tbFilm.Text}");
                    foreach (Film film in mainWindow.filmy)
                    {
                        if (film.id == Int32.Parse(tbFilm.Text))
                        {
                            film.je_rezervovan = true;
                        }
                    }
                }
            }
            mainWindow.dbconn.Insert($"INSERT INTO `T_Rezervace`(`Uzivatel`, `Film`, `Datum_platnosti_rezervace`) VALUES ({Int32.Parse(tbUzivatel.Text)}, {Int32.Parse(tbFilm.Text)}, \"{tbDatum.Text}\")");
            mainWindow.reader = mainWindow.dbconn.Select($"SELECT MAX(Id) FROM T_Rezervace WHERE 1");
            while (mainWindow.reader.Read())
            {
                Rezervace rezervaceJedna = new Rezervace(Int32.Parse(tbUzivatel.Text), Int32.Parse(tbFilm.Text), tbDatum.Text, mainWindow.reader.GetInt32(0));
                mainWindow.rezervace.Add(rezervaceJedna);
            }
            this.Close();
        }
    }
}
