﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1
{
    public class Rezervace
    {
        public int uzivatel;
        public int film;
        public string datum_platnosti;
        public int id;

        public Rezervace(int uzivatel, int film, string datum_platnosti, int id)
        {
            this.uzivatel = uzivatel;
            this.film = film;
            this.datum_platnosti = datum_platnosti;
            this.id = id;
        }

        public override string ToString()
        {
            return "ID rezervace: " + this.id.ToString();
        }

        public string getInfo()
        {
            return $"uzivatel: {uzivatel} \n film: {film} \n datum platnosti: {datum_platnosti} \n id: {id}";
        }
    }
}
