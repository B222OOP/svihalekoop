﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowUser.xaml
    /// </summary>
    public partial class WindowUser : Window
    {
        public MainWindow mainWindow;

        public WindowUser(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            lbFilmy.ItemsSource = mainWindow.filmy;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Film film = (Film)lbFilmy.SelectedItem;
            if (film != null)
            {
                tbFilmy.Text = film.getInfoUser(mainWindow);
            }
        }

        private void btnRezervace_Click(object sender, RoutedEventArgs e)
        {
            if (lbFilmy.SelectedItem != null)
            {
                Film film = (Film)lbFilmy.SelectedItem;
                if (film.je_rezervovan)
                {
                    MessageBox.Show("Film je rezervovan.");
                    return;
                }
                string date = DateTime.Now.AddDays(30).ToString("dd.MM.yyyy");
                mainWindow.dbconn.Insert($"INSERT INTO `T_Rezervace`(`Uzivatel`, `Film`, `Datum_platnosti_rezervace`) VALUES ({mainWindow.uzivatel.id}, {film.id}, \"{date}\")");
                mainWindow.reader = mainWindow.dbconn.Select($"SELECT MAX(Id) FROM T_Rezervace WHERE 1");
                while (mainWindow.reader.Read())
                {
                    Rezervace rezervaceJedna = new Rezervace(mainWindow.uzivatel.id, film.id, date, mainWindow.reader.GetInt32(0));
                    mainWindow.rezervace.Add(rezervaceJedna);
                }
                mainWindow.dbconn.Update($"UPDATE `T_Film` SET `Je_Rezervovan`= 1 WHERE Id={film.id}");
                foreach (Film film1 in mainWindow.filmy)
                {
                    if (film1.id == film.id)
                    {
                        film1.je_rezervovan = true;
                    }
                }
                tbFilmy.Text = film.getInfoUser(mainWindow);
            }
            lbFilmy.ItemsSource = null;
            lbFilmy.ItemsSource = mainWindow.filmy;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!tbJmeno.Text.Equals(""))
            {
                mainWindow.dbconn.Update($"UPDATE `T_Uzivatel` SET `Jmeno`=\"{tbJmeno.Text}\" WHERE Id={mainWindow.uzivatel.id}");
                mainWindow.uzivatel.jmeno = tbJmeno.Text;
            }
            if (!tbPrijmeni.Text.Equals(""))
            {
                mainWindow.dbconn.Update($"UPDATE `T_Uzivatel` SET `Prijmeni`=\"{tbPrijmeni.Text}\" WHERE Id={mainWindow.uzivatel.id}");
                mainWindow.uzivatel.prijmeni = tbPrijmeni.Text;
            }
            if (!tbPrezdivka.Text.Equals(""))
            {
                mainWindow.dbconn.Update($"UPDATE `T_Uzivatel` SET `Prezdivka`=\"{tbPrezdivka.Text}\" WHERE Id={mainWindow.uzivatel.id}");
                mainWindow.uzivatel.prezdivka = tbPrezdivka.Text;
            }
            if (!tbHeslo.Text.Equals(""))
            {
                mainWindow.dbconn.Update($"UPDATE `T_Uzivatel` SET `Heslo`=\"{tbHeslo.Text}\" WHERE Id={mainWindow.uzivatel.id}");
                mainWindow.uzivatel.heslo = tbHeslo.Text;
            }
            tbJmeno.Text = null;
            tbPrijmeni.Text = null;
            tbPrezdivka.Text = null;
            tbHeslo.Text = null;
        }
    }
}
