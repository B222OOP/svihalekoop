﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WpfApp1
{
    public class Film
    {
        public string nazev;
        public string zanr;
        public bool je_rezervovan;
        public int id;

        public Film(string nazev, string zanr, bool je_rezervovan, int id)
        {
            this.nazev = nazev;
            this.zanr = zanr;
            this.je_rezervovan = je_rezervovan;
            this.id = id;
        }

        public override string ToString()
        {
            return nazev;
        }

        public string getInfo()
        {
            return $"nazev: {nazev} \n zanr: {zanr} \n je_rezervovan: {je_rezervovan} \n id: {id}";
        }

        public string getInfoUser(MainWindow mainWindow)
        {
            if (je_rezervovan)
            {
                string datum = "";
                foreach (Rezervace rezervace in mainWindow.rezervace)
                {
                    if (rezervace.film == id)
                    {
                        datum = rezervace.datum_platnosti;
                    }
                }
                return $"nazev: {nazev} \n zanr: {zanr} \n je_rezervovan: {je_rezervovan} - do {datum}\n";
            }
            return $"nazev: {nazev} \n zanr: {zanr} \n je_rezervovan: {je_rezervovan} \n";
        }
    }
}
