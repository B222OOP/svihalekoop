﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            int[] pole = { 0, 1, 2, 3, 4 };
            int[] pole2 = (int[])pole.Clone();
            pole2[1] = 4;
            Console.WriteLine(pole[1]);

            ArrayList listik = new ArrayList();
            listik.Add("string");
            listik.Add(5.4);
            listik.Add(10);
            foreach(var o in listik)
            {
                Console.WriteLine(o);
            }

            GenerickaTrida<int> trida1 = new GenerickaTrida<int>(12);
            trida1.Vypis();
            GenerickaTrida<double> trida2 = new GenerickaTrida<double>(5.5);
            trida2.Vypis();
            

            // Write each directory name to a file.
            string path = @"C:\Users\svihadav.DESKTOP-ODPDVF5.005\Desktop\Nová složka (2)\svihalekoop\CV11\ConsoleApp1\Data\";
            string filename = "NasSoubor.txt";
            using (StreamWriter sw = new StreamWriter(path + filename, append:true))
            {
                sw.WriteLine("Ahoj, svete");
            }

            using (StreamReader sr = new StreamReader(path + filename))
            {
                Console.WriteLine(sr.ReadToEnd());
            }

            Dictionary<string, string> slovnik = new Dictionary<string, string>();
            slovnik.Add("klic", "toto je hodnota");
            Console.WriteLine(slovnik["klic"]);

            Dictionary<string, dynamic> slovnik2 = new Dictionary<string, dynamic>();
            slovnik2.Add("1", "hello");
            slovnik2.Add("2", 2);

            foreach(var o in slovnik2)
            {
                Console.WriteLine(String.Format("Klic ma hodnotu: {0} a jmeno {1}", o.Value, o.Key));
            }
            */

            Dictionary<string, dynamic> slovnik = new Dictionary<string, dynamic>();
            for (int i = 0; i < 10; ++i)
            {
                slovnik.Add(i.ToString(), i.ToString() + " a nejaky text");
            }

            string path = @"C:\Users\svihadav.DESKTOP-ODPDVF5.005\Desktop\Nová složka (2)\svihalekoop\CV11\ConsoleApp1\Data\";
            string filename = "slovnik.txt";
            using (StreamWriter sw = new StreamWriter(path + filename))
            {
                foreach (var o in slovnik)
                {
                    sw.WriteLine(o.Key + "\n" + o.Value);
                }
            }

            Dictionary<string, dynamic> slovnikZeSouboru = new Dictionary<string, dynamic>();

            using (StreamReader sr = new StreamReader(path + filename))
            {
                slovnikZeSouboru = new Dictionary<string, dynamic>(udelejSlovnik(sr, path, filename));
            }

            foreach (var o in slovnikZeSouboru)
            {
                Console.WriteLine(o.Key + " : " + o.Value);
            }


            Console.ReadKey();
        }

        public static int KolikMaRadkuSoubor(StreamReader sr)
        {
            int i = 0;
            string str = sr.ReadLine();
            while (str != null && !str.Equals("\n")) { ++i; str = sr.ReadLine(); }
            return i;
        }

        public static Dictionary<string, dynamic> udelejSlovnik(StreamReader sr, string path, string filename)
        {
            Dictionary<string, dynamic> slovnik = new Dictionary<string, dynamic>();
            int pocetRadku = KolikMaRadkuSoubor(sr) / 2;

            using (StreamReader sr2 = new StreamReader(path + filename))
            {
                for (int i = 0; i < pocetRadku; ++i)
                {
                    string radek1 = sr2.ReadLine();
                    string radek2 = sr2.ReadLine();
                    if (radek1 == null || radek2 == null || radek1.Equals("\n") || radek2.Equals("\n"))
                    {
                        continue;
                    }
                    slovnik.Add(radek1, radek2);

                }
            }
            return slovnik;
        }
    }

    public class GenerickaTrida<T>
    {
        private T variable;

        public GenerickaTrida(T variable)
        {
            this.variable = variable;
        }

        public void Vypis()
        {
            Console.WriteLine(String.Format("Promenna ma hodnotu: {0} a hodnotu: {1}",variable, variable.GetType()));
        }
    }

}
