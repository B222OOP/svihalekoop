﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using MySqlConnector;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*XDocument xdoc = new XDocument(
                new XElement("patients",
                    new XElement("patient",
                        new XAttribute("name", "Tomas"),
                        new XAttribute("surname", "Jedno"),
                            new XElement("Mereni", "neco"))));

            string filePath = @"C:\Users\svihadav.DESKTOP-ODPDVF5.006\Desktop\Nová složka\ConsoleApp1\ConsoleApp1";
            xdoc.Save(filePath);

            XDocument xdoc1 = XDocument.Load(filePath);

            foreach(var node in xdoc1.DescendantNodes())
            {
                Console.WriteLine(
                    String.Format("V promenne se nachazi: {0} a je to typu {1}", node, node.GetType()));
                if (node is XText)
                {

                }
            } */

            // XAMPP

            List<Patient> patientList = new List<Patient>();

            DB_connect dbconn;
            MySqlDataReader reader;

            dbconn.Insert("INSERT INTO `Patient`(`Name`, `Surname`, `PojistovnaID`, `RC`, `Adresa1_ID`) VALUES (\"Test\", \"Insertu\", 1, 1231332, 2);");
            reader = dbconn.Select("SELECT Name, Surname, PojistovnaID, RC, Adresa1_ID FROM Patient");
            while (reader.Read())
            {
                Console.WriteLine(String.Format("{0} {1} {2} {3} {4}", reader.GetString(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4)));
                // vytvorit pacienty a dat je do listu
                Patient patient = new Patient(reader.GetString(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4));
                patientList.Add(patient);
            }
            Console.WriteLine("");
            dbconn.CloseConn();

            // foreachem vypsat pacienty
            foreach(var patient in patientList)
            {
                Console.WriteLine(patient.Name + " " + patient.Surname + " " + patient.PojistovnaID + " " + patient.RC + " " + patient.Adresa1_ID);
            }
            
            Console.ReadKey();
        }
    }
}
