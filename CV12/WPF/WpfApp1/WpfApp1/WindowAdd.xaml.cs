﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowAdd.xaml
    /// </summary>
    public partial class WindowAdd : Window
    {
        public Patient patient;
        public WindowAdd()
        {
            InitializeComponent();
        }

        private void btnHotovo_Click(object sender, RoutedEventArgs e)
        {
            if (!tb1.Text.Equals("") && !tb2.Text.Equals("") && !tb3.Text.Equals("") && !tb4.Text.Equals("") && !tb5.Text.Equals(""))
            {
                patient = new Patient(tb1.Text, tb2.Text, Int32.Parse(tb3.Text), Int32.Parse(tb4.Text), Int32.Parse(tb5.Text));
            }
            this.Close();
        }
    }
}
