﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class Patient
    {
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public int PojistovnaID { get; private set; }
        public int RC { get; private set; }
        public int Adresa1_ID { get; private set; }

        public Patient(string Name, string Surname, int PojistovnaID, int RC, int Adresa1_ID)
        {
            this.Name = Name;
            this.Surname = Surname;
            this.PojistovnaID = PojistovnaID;
            this.RC = RC;
            this.Adresa1_ID = Adresa1_ID;
        }

        public override string ToString()
        {
            return Name + " " + Surname + " " + PojistovnaID + " " + RC + " " + Adresa1_ID;
        }
    }
}