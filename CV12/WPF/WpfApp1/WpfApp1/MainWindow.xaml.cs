﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Patient> listPatient = new List<Patient>();
        DB_connect dbconn;
        MySqlDataReader reader;

        public MainWindow()
        {
            InitializeComponent();
            lboxKartoteka.ItemsSource = listPatient;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            WindowAdd windowAdd = new WindowAdd();
            windowAdd.ShowDialog();
            listPatient.Add(windowAdd.patient);
        }

        private void resetListBox()
        {
            lboxKartoteka.ItemsSource = null;
            lboxKartoteka.ItemsSource = listPatient;
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            resetListBox();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lboxKartoteka.SelectedIndex != -1)
            {
                listPatient.Remove((Patient)lboxKartoteka.SelectedItem);
            }
        }
    }
}
