﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;


namespace CV03
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Fighter> listbojovniku = new List<Fighter>();
            /*Fighter fighter1 = new Fighter("Marcel", 17, 6, 120, 20);
            Fighter fighter2 = new Fighter("Karel", 11, 14, 145, 8);
            listbojovniku.Add(fighter1);
            listbojovniku.Add(fighter2);
            listbojovniku.Add(new Fighter("Petr", 14, 9, 120, 14));
            listbojovniku.Add(new Fighter("Pavel", 14, 9, 120, 14));
            listbojovniku.Add(new Fighter("Antonin", 14, 9, 120, 14));
            listbojovniku.Add(new Fighter("Tonda", 14, 9, 120, 14));
            listbojovniku.Add(new Fighter("Jozef", 14, 9, 120, 14));
            listbojovniku.Add(new Fighter("Pepik", 14, 9, 120, 14));
            Tournament turnaj = new Tournament(listbojovniku);
            turnaj.start();*/

            string jsonFilePath = @"C:\Users\svihadav.DESKTOP-ODPDVF5.007\Desktop\Nová složka\svihalekoop\CV13\CV03\Bojovnici.json";
            string json = File.ReadAllText(jsonFilePath);
            var serializer = new JavaScriptSerializer(); //using System.Web.Script.Serialization;

            Dictionary<string, dynamic> json_Dictionary = serializer.Deserialize<Dictionary<string, dynamic>>(json);

            foreach (var item in json_Dictionary)
            {
               // Console.WriteLine(String.Format("Klic:{0} value:{1}", item.Key, item.Value));

                foreach(var item2 in item.Value)
                {
                    // Console.WriteLine(String.Format("Klic:{0}", item2));

                    string name = "";
                    bool jmenoDone = false;
                    int attack = 0;
                    bool attackDone = false;
                    int def = 0;
                    bool defDone = false;
                    int hp = 0;
                    bool hpDone = false;
                    int INT = 0;
                    bool INTDone = false;
                    bool invalidHodnota = false;

                    foreach (var item3 in item2)
                    {
                       // Console.WriteLine(String.Format("Klic:{0} value:{1}", item3.Key, item3.Value));
                        switch (item3.Key)
                        {
                            case "jmeno":
                                name = item3.Value;
                                jmenoDone = true;
                                break;
                            case "attack":
                                attack = item3.Value;
                                attackDone = true;
                                break;
                            case "deff":
                                def = item3.Value;
                                defDone = true;
                                break;
                            case "hp":
                                hp = item3.Value;
                                hpDone = true;
                                break;
                            case "INT":
                                INT = item3.Value;
                                INTDone = true;
                                break;
                            default:
                                invalidHodnota = true;
                                break;
                        }
                    }
                    if (jmenoDone && attackDone && defDone && hpDone && INTDone && !invalidHodnota)
                    {
                        Fighter fighter = new Fighter(name,attack,def,hp,INT);
                        listbojovniku.Add(fighter);
                    }
                    else
                    {
                        Console.WriteLine("Bojovnik nenacten z jsonu." + jmenoDone + attackDone + defDone + hpDone + INTDone + invalidHodnota);
                    }
                }
            }

            DB_connect dbconn = new DB_connect();
            MySqlDataReader reader;

            foreach (var fighter in listbojovniku)
            {
                Console.WriteLine(fighter.jmeno + " " + fighter.attack + " " + fighter.deff + " " + fighter.hp + " " + fighter.INT);
                dbconn.Insert($"INSERT INTO `Bojovnik`(`jmeno`, `attack`, `deff`, `hp`, `INTT`) VALUES (\"{fighter.jmeno}\", {fighter.attack}, {fighter.deff}, {fighter.hp}, {fighter.INT});");
            }
            /*reader = dbconn.Select("SELECT Name, Surname, PojistovnaID, RC, Adresa1_ID FROM Patient");
            while (reader.Read())
            {
                Console.WriteLine(String.Format("{0} {1} {2} {3} {4}", reader.GetString(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4)));
                // vytvorit pacienty a dat je do listu
                Patient patient = new Patient(reader.GetString(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4));
                patientList.Add(patient);
            }*/

            // Pro save
            var output = serializer.Serialize(json_Dictionary);
            File.WriteAllText(@"C:\Users\svihadav.DESKTOP-ODPDVF5.007\Desktop\Nová složka\svihalekoop\CV13\CV03\Bojovnici2.json", output);

            Console.WriteLine("");
            dbconn.CloseConn();



























            // Try catch bloky

            try
            {
                int cena = Int32.Parse(Console.ReadLine());
                int[] pole = { 5, 3 };
                pole[3] += 4;
            }
            catch (OverflowException)
            {
                Console.WriteLine("Zadana hodnota je prilis velka");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Toto misto v poli neni");
            }
            catch (FormatException)
            {
                Console.WriteLine("Nespravny format");
            }




            Console.ReadKey();

        }
    }
}
