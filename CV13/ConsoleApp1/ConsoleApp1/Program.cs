﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Zadejte pocet vetvi.");
                string input = Console.ReadLine();
                int pocetVetvi;

                try
                {
                    pocetVetvi = Int32.Parse(input);
                    if (pocetVetvi % 2 == 0)
                    {
                        throw new Exception();
                    }
                }
                catch
                {
                    Console.WriteLine("Nespravne zadana hodnota.");
                    continue;
                }

                int maxPocetHvezd = 1;

                for (int i = 2; i <= pocetVetvi; ++i)
                {
                    maxPocetHvezd += 2;
                }

                int index = 0;

                for (int i = 0; i < pocetVetvi; ++i)
                {
                    for (int j = 0; j < maxPocetHvezd / 2 - index; ++j)
                    {
                        Console.Write(" ");
                    }
                    ++index;
                    for (int j = 0; j < i + index; ++j)
                    {
                        Console.Write("*");
                    }
                    for (int j = 0; j < maxPocetHvezd / 2 - index + 1; ++j)
                    {
                        Console.Write(" ");
                    }
                    Console.WriteLine();
                }

                for (int i = 0; i < pocetVetvi / 5 + 1; ++i)
                {
                    for (int j = 0; j < maxPocetHvezd / 2 - 1; ++j)
                    {
                        Console.Write(" ");
                    }
                    for (int j = 0; j < 3; ++j)
                    {
                        Console.Write("*");
                    }
                    for (int j = 0; j < maxPocetHvezd / 2 - 1; ++j)
                    {
                        Console.Write(" ");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
