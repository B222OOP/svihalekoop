﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool klikloSeNaRovnaSe = false;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            double number = 0;
            if (klikloSeNaRovnaSe)
            {
                try
                {
                    number += double.Parse(tb2.Text);
                }
                catch
                {
                    return;
                }
                klikloSeNaRovnaSe = false;
                tb1.Text = number.ToString();
                tb2.Text = "";
            }
            else
            {
                try
                {
                    number = double.Parse(tb1.Text) + double.Parse(tb2.Text);
                }
                catch
                {
                    return;
                }
                tb1.Text = number.ToString();
                tb2.Text = "";
            }

            /*int number = 0;
            try
            {
                number = int.Parse(tb1.Text);
            }
            catch
            {
                return;
            }

            if (number != 1)
            {
                lbl.Content += "," + number;
            }
            else
            {
                lbl.Content = number.ToString();
            }
            ++number;
            tb1.Text = number.ToString();

            if (lbl.Content.ToString() == "Ahoj svete")
            {
                lbl.Content = "neco jineho";
            }
            else
            {
                lbl.Content = "Ahoj svete";
            }*/
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            double number = 0;
            if (klikloSeNaRovnaSe)
            {
                try
                {
                    number -= double.Parse(tb2.Text);
                }
                catch
                {
                    return;
                }
                klikloSeNaRovnaSe = false;
                tb1.Text = number.ToString();
                tb2.Text = "";
            }
            else
            {
                try
                {
                    number = double.Parse(tb1.Text) - double.Parse(tb2.Text);
                }
                catch
                {
                    return;
                }
                tb1.Text = number.ToString();
                tb2.Text = "";
            }
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            double number = 0;
            if (klikloSeNaRovnaSe)
            {
                try
                {
                    number *= double.Parse(tb2.Text);
                }
                catch
                {
                    return;
                }
                klikloSeNaRovnaSe = false;
                tb1.Text = number.ToString();
                tb2.Text = "";
            }
            else
            {
                try
                {
                    number = double.Parse(tb1.Text) * double.Parse(tb2.Text);
                }
                catch
                {
                    return;
                }
                tb1.Text = number.ToString();
                tb2.Text = "";
            }
        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            double number = 0;
            if (klikloSeNaRovnaSe)
            {
                try
                {
                    number /= double.Parse(tb2.Text);
                }
                catch
                {
                    return;
                }
                klikloSeNaRovnaSe = false;
                tb1.Text = number.ToString();
                tb2.Text = "";
            }
            else
            {
                try
                {
                    number = double.Parse(tb1.Text) / double.Parse(tb2.Text);
                }
                catch
                {
                    return;
                }
                tb1.Text = number.ToString();
                tb2.Text = "";
            }
        }

        private void btn5_Click(object sender, RoutedEventArgs e)
        {
            double number = 0;
            if (klikloSeNaRovnaSe)
            {
                try
                {
                    number %= double.Parse(tb2.Text);
                }
                catch
                {
                    return;
                }
                klikloSeNaRovnaSe = false;
                tb1.Text = number.ToString();
                tb2.Text = "";
            }
            else
            {
                try
                {
                    number = double.Parse(tb1.Text) % double.Parse(tb2.Text);
                }
                catch
                {
                    return;
                }
                tb1.Text = number.ToString();
                tb2.Text = "";
            }
        }

        private void btn6_Click(object sender, RoutedEventArgs e)
        {
            klikloSeNaRovnaSe = true;
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = "0";
            tb2.Text = "";
            klikloSeNaRovnaSe = true;
        }
    }
}
