﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Point position;
        private Point positionRct = new Point();
        private Rectangle rct;
        private DispatcherTimer dispatcherTimer;
        private int posun = 6;
        private bool jedemeDopredu = true;

        public MainWindow()
        {
            InitializeComponent();
            //  DispatcherTimer setup
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
        }

        public bool prekryvaSeRectangle(List<double> positionsX, List<double> positionsY, double X, double Y, Rectangle r)
        {
            int pocetPositionu = positionsX.Count();
            for (int i = 0; i < pocetPositionu; ++i)
            {
                if ( (X >= positionsX[i] && X <= positionsX[i] + r.Width) || (Y >= positionsY[i] && Y <= positionsY[i] + r.Height))
                {
                    return true;
                }
            }

            return false;
        }

        public void addRectangle()
        {
            // vytvoreni rectangle
            rct = new Rectangle();
            rct.Fill = new SolidColorBrush(Colors.Red);
            rct.Width = 40;
            rct.Height = 20;
            Canvas.SetLeft(rct, 0);
            Canvas.SetTop(rct, 50);
            cnv.Children.Add(rct);

            // 4 rectangly
            Rectangle rct1 = new Rectangle();
            Rectangle rct2 = new Rectangle();
            Rectangle rct3 = new Rectangle();
            Rectangle rct4 = new Rectangle();

            List<Rectangle> rectangles = new List<Rectangle>{ rct1, rct2, rct3, rct4 };

            // list pozic na kterych budou rectangly umisteny
            List<double> positionsX = new List<double>();
            List<double> positionsY = new List<double>();

            Random rnd = new Random();


            // umisteni rectanglu na platno
            foreach(Rectangle r in rectangles)
            {
                // nastaveni rectanglu
                r.Fill = new SolidColorBrush(Colors.Blue);
                r.Width = 40;
                r.Height = 20;

                // vypocitej startovni pozici rectanglu, ale ne takovou ktera by se prekryvala s existujicim rectanglem
                do
                {
                    this.position.X = rnd.Next(0, (int)cnv.Width - (int)r.Width);
                    this.position.Y = rnd.Next(0, (int)cnv.Height - (int)r.Height);
                }
                while (prekryvaSeRectangle(positionsX, positionsY, this.position.X, this.position.Y, r)); // prekryvaSeRectangle(positionsX, positionsY, this.position.X, this.position.Y, r)

                // uspesna pozice
                positionsX.Add(this.position.X);
                positionsY.Add(this.position.Y);

                // nastav pozici rectanglu
                Canvas.SetLeft(r, this.position.X);
                Canvas.SetTop(r, this.position.Y);


                // zobraz rectangle na platne
                cnv.Children.Add(r);
            }

            // pozice canvasu

            // zarazeni do canvasu
            //cnv.Children.Add(rct);
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            cnv.Children.Clear();
            this.positionRct.X = 0;
            dispatcherTimer.Start();
            addRectangle();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (jedemeDopredu)
            {
                if (this.positionRct.X + rct.Width + posun <= cnv.Width)
                {
                    this.positionRct.X += posun;
                }
                else
                {
                    this.positionRct.X -= posun;
                    jedemeDopredu = false;
                }
            }
            else
            {
                if (this.positionRct.X - posun >= 0)
                {
                    this.positionRct.X -= posun;
                }
                else
                {
                    this.positionRct.X += posun;
                    jedemeDopredu = true;
                }
            }
            Canvas.SetLeft(rct, this.positionRct.X);
        }
    }
}
