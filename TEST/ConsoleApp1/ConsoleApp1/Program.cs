﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Patient pacient1 = new Patient("Karel", "Kamen", 12340568, 90, 200);
            Patient pacient2 = new Patient("Jan", "Amos", 90449933, 100, 170);
            Patient pacient3 = new Patient("Robert", "Ryn", 19349955, 65, 172);
            Patient pacient4 = new Patient("Lukas", "Lofata", 09091233, 120, 200);

            List<Patient> pacienti = new List<Patient>();

            pacienti.Add(pacient1);
            pacienti.Add(pacient2);
            pacienti.Add(pacient3);
            pacienti.Add(pacient4);

            List<Room> pokoje = new List<Room>();

            Room pokoj1 = new Room(3, 250);
            Room pokoj2 = new Room(2, 110);
            Room pokoj3 = new Room(1, 90);

            pokoje.Add(pokoj1);
            pokoje.Add(pokoj2);
            pokoje.Add(pokoj3);

            pokoj1.PridatPacientaNaLuzko(pacienti[0]);
            pokoj1.VypisPacientyVPokoji();

            pokoj1.OdebratPacientaZLuzka(1);
            pokoj1.VypisPacientyVPokoji();

            pokoj3.PridatPacientaNaLuzko(pacienti[3]);
            pokoj3.VypisPacientyVPokoji();

            pokoj3.PridatPacientaNaLuzko(pacienti[2]);
            pokoj3.PridatPacientaNaLuzko(pacienti[0]);
            pokoj3.VypisPacientyVPokoji();

            pokoj1.VycistiPokoj();
            pokoj2.VycistiPokoj();
            pokoj3.VycistiPokoj();

            Console.WriteLine("-----------------");


            pokoj1.VypisPacientyVPokoji();
            pokoj2.VypisPacientyVPokoji();
            pokoj3.VypisPacientyVPokoji();

            Console.WriteLine("-----------------");

            pacienti = pacienti.OrderByDescending(o => o.GetHmotnost()).ToList();
            pokoje = pokoje.OrderBy(o => o.getMaxHmotnostLuzek()).ToList();


            foreach (Patient pacient in pacienti)
            {
                foreach (Room pokoj in pokoje)
                {
                    if (pokoj.PridatPacientaNaLuzko(pacient))
                    {
                        break;
                    }
                }

                if (!pacient.GetJePacientem())
                {
                    Console.WriteLine("Pacienta nelze umistit na zadny z pokoju");
                }
            }

            foreach (Room pokoj in pokoje)
            {
                pokoj.VypisPacientyVPokoji();
            }

            pokoj1.VycistiPokoj();
            pokoj2.VycistiPokoj();
            pokoj3.VycistiPokoj();

            Console.WriteLine("-----------------");


            pokoj1.VypisPacientyVPokoji();
            pokoj2.VypisPacientyVPokoji();
            pokoj3.VypisPacientyVPokoji();

            Console.WriteLine("-----------------");

            string input;
            do
            {
                Console.WriteLine("Zadej prikaz: (\"help\" pro seznam prikazu, \"konec\" pro konec): ");

                input = Console.ReadLine();

                if (input.Equals("help"))
                {
                    Console.WriteLine("pan programator zde zatim nic neudelal.");
                }
            }
            while (!input.Equals("konec"));
        }
    }
}
