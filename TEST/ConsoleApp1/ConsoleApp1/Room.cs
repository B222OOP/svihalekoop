﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Room
    {
        private int pocetLuzek;
        private int maxHmotnostLuzek;
        private List<Bed> luzka = new List<Bed>();

        public Room(int pocetLuzek, int maxHmotnostLuzek)
        {
            this.pocetLuzek = pocetLuzek;
            this.maxHmotnostLuzek = maxHmotnostLuzek;

            for (int i = 0; i < pocetLuzek; ++i)
            {
                Bed luzko = new Bed(maxHmotnostLuzek);
                luzka.Add(luzko);
            }
        }

        public bool PridatPacientaNaLuzko(Patient pacient)
        {
            if (pacient.GetHmotnost() <= maxHmotnostLuzek)
            {
                for (int i = 0; i < pocetLuzek; ++i)
                {
                    if (!luzka[i].GetObsazeno())
                    {
                        luzka[i].PridatPacienta(pacient);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool OdebratPacientaZLuzka(int cisloLuzka, bool nezaleziNaBMI = false)
        {
            if (cisloLuzka <= 0 || cisloLuzka > pocetLuzek)
            {
                return false;
            }

            if (luzka[cisloLuzka - 1].GetObsazeno() && (nezaleziNaBMI || luzka[cisloLuzka - 1].GetPatient().maBMIVNormalu()))
            {
                luzka[cisloLuzka - 1].OdebratPacienta();
                return true;
            }

            return false;
        }

        public void VycistiPokoj()
        {
            for (int i = 1; i <= pocetLuzek; ++i)
            {
                OdebratPacientaZLuzka(i, true);
            }
        }

        public void VypisPacientyVPokoji()
        {
            Console.WriteLine("Seznam pacientu na pokoji: ");
            for (int i = 0; i < pocetLuzek; ++i)
            {
                if (luzka[i].GetObsazeno())
                {
                    luzka[i].GetPatient().VypisPacienta();
                }
            }
        }

        public int getMaxHmotnostLuzek()
        {
            return maxHmotnostLuzek;
        }

    }
}
