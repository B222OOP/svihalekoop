﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Bed
    {
        private int nosnost;
        private bool obsazeno = false;
        private Patient pacient;

        public Bed(int nosnost)
        {
            this.nosnost = nosnost;
        }

        public void PridatPacienta(Patient pacient)
        {
            if (!obsazeno)
            {
                this.pacient = pacient;
                pacient.stalSePacientem();
                obsazeno = true;
            }
        }

        public void OdebratPacienta()
        {
            if (obsazeno)
            {
                pacient.jizNeniPacientem();
                this.pacient = null;
                obsazeno = false;
            }
        }

        public bool GetObsazeno()
        {
            return this.obsazeno;
        }

        public Patient GetPatient()
        {
            return this.pacient;
        }
    }
}
