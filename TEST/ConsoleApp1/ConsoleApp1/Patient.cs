﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Patient
    {
        private string jmeno;
        private string prijmeni;
        private int rodneCislo;
        private int hmotnost;
        private int vyska;
        private bool jePacientem;

        public Patient(string jmeno, string prijmeni, int rodneCislo, int hmotnost, int vyska, bool jePacientem = false)
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.rodneCislo = rodneCislo;
            this.hmotnost = hmotnost;
            this.vyska = vyska;
            this.jePacientem = jePacientem;
        }

        public void Zhubnout(int oKolikKg)
        {
            this.hmotnost -= oKolikKg;
        }

        public void Pribrat(int oKolikKg)
        {
            this.hmotnost += oKolikKg;
        }

        public int GetHmotnost()
        {
            return hmotnost;
        }

        public void VypisPacienta()
        {
            Console.WriteLine($"Pacient: {this.jmeno} {this.prijmeni}, rodne cislo: {this.rodneCislo}, hmotnost: {this.hmotnost}, vyska: {this.vyska}, BMI: {VypoctiBMI()}");
        }

        public double VypoctiBMI()
        {
            return hmotnost / Math.Pow((this.vyska / 100.0), 2);
        }

        public bool maBMIVNormalu()
        {
            double BMI = VypoctiBMI();
            if (BMI >= 18.5 && BMI <= 24.9)
            {
                return true;
            }
            return false;
        }

        public void stalSePacientem()
        {
            this.jePacientem = true;
        }

        public void jizNeniPacientem()
        {
            this.jePacientem = false;
        }

        public bool GetJePacientem()
        {
            return jePacientem;
        }
    }
}
