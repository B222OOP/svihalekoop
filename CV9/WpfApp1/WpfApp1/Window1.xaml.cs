﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        MainWindow mainWindow;

        public Window1(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
        }

        private void btnPridejPsa_Click(object sender, RoutedEventArgs e)
        {
            if (!txtBoxJmeno.Text.Equals("") && !txtBoxVek.Text.Equals("") && !txtBoxRasa.Text.Equals(""))
            {
                Dog dog = new Dog(txtBoxJmeno.Text, Int32.Parse(txtBoxVek.Text), txtBoxRasa.Text);

                mainWindow.listPsu.Add(dog);

                mainWindow.lbox.Items.Add(dog.name);

                this.Close();

            }
        }
    }
}
