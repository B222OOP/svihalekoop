﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static List<Dog> listPsu = new List<Dog>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            List<string> listStringu = new List<string>(new string[] { "jedna", "dva", "tri", "ctyri", "pet", "sest", "sedm", "osm", "devet", "10" });
            
            foreach (var o in listStringu)
            {
                lbox.Items.Add(o);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            lbox.Items.Remove(lbox.SelectedItem);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!txtBox.Text.Equals(""))
            {
                lbox.Items.Add(txtBox.Text);
                txtBox.Text = "";
            }
        }

        private void btnNovyPes_Click(object sender, RoutedEventArgs e)
        {
            Window1 window1 = new Window1(this);
            window1.ShowDialog();
        }
    }
}
