﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1
{
    public class Dog
    {
        public string name;
        public int age;
        public string breed;

        public Dog(string name, int age, string breed)
        {
            this.name = name;
            this.age = age;
            this.breed = breed;
        }
    }
}
