﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Pes:Zvire
    {
        public string rasa;

        public Pes(string jmeno, int vek, int pocetNohou, string rasa, string barva):base(jmeno, vek, pocetNohou, barva)
        {
            this.rasa = rasa;
        }

        public override string barva { get => base.barva+"Overrided"; set => base.barva = value; }

        public override string Zarvi()
        {
            return "Takhle steka pejsek.";
        }

        public void ZkusMetody()
        {
            Console.WriteLine(this.GetJmeno() + " " + this.getPocetNohou());
        }
    }
}
