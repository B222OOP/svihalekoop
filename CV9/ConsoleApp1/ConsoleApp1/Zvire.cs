﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Zvire
    {
        public string jmeno;
        public int vek;
        public int pocetNohou;

        public virtual string barva { get; set; }

        public Zvire(string jmeno, int vek, int pocetNohou, string barva)
        {
            this.jmeno = jmeno;
            this.vek = vek;
            this.pocetNohou = pocetNohou;
            this.barva = barva;
        }

        public virtual string Zarvi()
        {
            return "Takhle rve zvire.";
        }

        public string GetJmeno()
        {
            return this.jmeno;
        }

        private int getVek()
        {
            return this.vek;
        }

        protected int getPocetNohou()
        {
            return this.pocetNohou;
        }
    }
}
