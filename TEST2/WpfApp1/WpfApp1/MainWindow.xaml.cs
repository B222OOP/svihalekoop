﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Pokoj> pokoje = new List<Pokoj>();
        private List<Postel> postele = new List<Postel>();
        private List<Pacient> pacienti = new List<Pacient>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnPokoj_Click(object sender, RoutedEventArgs e)
        {
            WindowPokoj windowPokoj = new WindowPokoj(this);
            windowPokoj.ShowDialog();

            pokoje.Add(windowPokoj.GetPokoj());

            lbPokoj.ItemsSource = null;
            lbPokoj.ItemsSource = pokoje;

        }

        public List<Pokoj> GetPokoje()
        {
            return pokoje;
        }

        private void btnPacient_Click(object sender, RoutedEventArgs e)
        {
            WindowPacient windowPaciet = new WindowPacient(this);
            windowPaciet.ShowDialog();

            pacienti.Add(windowPaciet.GetPacient());

            lbPacient.ItemsSource = null;
            lbPacient.ItemsSource = pacienti;
        }

        private void lbPokoj_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbPokoj.SelectedItem != null)
            {
                Pokoj pokoj = (Pokoj)lbPokoj.SelectedItem;
                lbPostel.Visibility = Visibility.Visible;
                lbPostel.ItemsSource = null;
                lbPostel.ItemsSource = pokoj.GetPostele();
                postele = pokoj.GetPostele();
            }
            else
            {
                return;
            }
        }

        private void btnPresunZKartoteky_Click(object sender, RoutedEventArgs e)
        {
            if (lbPacient.SelectedItem == null || lbPostel.SelectedItem == null)
            {
                lblError.Content = "Nevybral jste pacienta ci luzko";
                return;
            }

            Postel postel= (Postel)lbPostel.SelectedItem;


            if (postel.GetJeObsazena())
            {
                lblError.Content = "Na luzku jiz lezi pacient";
                return;
            }

            Pacient pacient = (Pacient)lbPacient.SelectedItem;

            if (postel.GetNosnost() < pacient.GetVaha())
            {
                lblError.Content = "Pacient je prilis obezni pro toto luzko";
                return;
            }

            lblError.Content = "";

            pacienti.Remove(pacient);

            postel.PridejPacienta(pacient);

            lbPacient.ItemsSource = null;
            lbPacient.ItemsSource = pacienti;

            Pokoj pokoj = (Pokoj)lbPokoj.SelectedItem;

            lbPostel.ItemsSource = null;
            lbPostel.ItemsSource = pokoj.GetPostele();

        }

        private void btnPresunZLuzka_Click(object sender, RoutedEventArgs e)
        {
            if (lbPostel.SelectedItem == null)
            {
                lblError.Content = "Nevybral jste pacienta";
                return;
            }

            Postel postel = (Postel)lbPostel.SelectedItem;

            if (!postel.GetJeObsazena())
            {
                lblError.Content = "Na luzku nelezi zadny pacient";
                return;
            }

            lblError.Content = "";

            Pacient pacient = postel.GetPacient();

            pacienti.Add(pacient);

            postel.OdeberPacienta();

            lbPacient.ItemsSource = null;
            lbPacient.ItemsSource = pacienti;

            Pokoj pokoj = (Pokoj)lbPokoj.SelectedItem;

            lbPostel.ItemsSource = null;
            lbPostel.ItemsSource = pokoj.GetPostele();
        }
    }
}
