﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1
{
    public class Postel
    {
        private int nosnost { get; set; }
        private string jmeno { get; set; }
        private bool jeObsazena { get; set; }
        private Pacient pacient { get; set; }

        public Postel(int nosnost, string jmeno, bool jeObsazena = false)
        {
            this.nosnost = nosnost;
            this.jmeno = jmeno;
            this.jeObsazena = jeObsazena;
        }

        public void PridejPacienta(Pacient pacient)
        {
            this.pacient = pacient;
            this.jeObsazena = true;
        }

        public void OdeberPacienta()
        {
            this.pacient = null;
            this.jeObsazena = false;
        }

        public int GetNosnost()
        {
            return nosnost;
        }

        public Pacient GetPacient()
        {
            return pacient;
        }

        public bool GetJeObsazena()
        {
            return jeObsazena;
        }

        public override string ToString()
        {
            if (jeObsazena)
            {
                return jmeno + " - " + pacient.ToString();
            }
            return jmeno;
        }
    }
}
