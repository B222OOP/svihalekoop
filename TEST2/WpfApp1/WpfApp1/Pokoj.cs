﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1
{

    public class Pokoj
    {
        private int pocetLuzek;
        private List<Postel> postele = new List<Postel>();
        private int cisloPokoje;

        public Pokoj(int pocetLuzek, int nosnost, int cisloPokoje)
        {
            for (int i = 0; i < pocetLuzek; ++i)
            {
                postele.Add(new Postel(nosnost, "L" + (i+1).ToString()));
            }

            this.cisloPokoje = cisloPokoje;
        }

        public int getPocetLuzek()
        {
            return pocetLuzek;
        }

        public List<Postel> GetPostele()
        {
            return postele;
        }

        public override string ToString()
        {
            return "Pokoj" + cisloPokoje.ToString();
        }
    }
}
