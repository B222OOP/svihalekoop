﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowPacient.xaml
    /// </summary>
    public partial class WindowPacient : Window
    {
        private Pacient pacient;
        private MainWindow mainWindow;

        public WindowPacient(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            InitializeComponent();
        }

        private void btnPacient_Click(object sender, RoutedEventArgs e)
        {
            if (tbJmeno.Text.Equals("") || tbPrijmeni.Text.Equals("") || tbRc.Text.Equals(""))
            {
                lbError.Content = "Nespravne zadane udaje.";
                return;
            }

            int vaha;
            int vyska;

            try
            {
                vaha = Int32.Parse(tbVaha.Text);
                vyska = Int32.Parse(tbVyska.Text);
            }
            catch
            {
                lbError.Content = "Nespravne zadane udaje.";
                return;
            }

            pacient = new Pacient(tbJmeno.Text, tbPrijmeni.Text, tbRc.Text, vaha, vyska);
            lbError.Content = "";
            Close();
        }

        public Pacient GetPacient()
        {
            return pacient;
        }
    }
}
