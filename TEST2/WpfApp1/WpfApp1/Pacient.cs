﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1
{
    public class Pacient
    {
        private string jmeno;
        private string prijmeni;
        private string rc;
        private int vaha;
        private int vyska;

        public Pacient(string jmeno, string prijmeni, string rc, int vaha, int vyska)
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.rc = rc;
            this.vaha = vaha;
            this.vyska = vyska;
        }

        public override string ToString()
        {
            return $"{jmeno} {prijmeni}";
        }

        public int GetVaha()
        {
            return vaha;
        }
    }
}
