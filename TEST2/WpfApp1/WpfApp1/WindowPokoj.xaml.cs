﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowPokoj.xaml
    /// </summary>
    public partial class WindowPokoj : Window
    {
        private Pokoj pokoj;
        MainWindow mainWindow;

        public WindowPokoj(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            InitializeComponent();
        }

        private void btnPokoj_Click(object sender, RoutedEventArgs e)
        {
            int pocetLuzek;
            int nosnost;
            try
            {
                pocetLuzek = Int32.Parse(tbPocetLuzek.Text);
                nosnost = Int32.Parse(tbNosnost.Text);
            }
            catch
            {
                lbError.Content = "Zadejte prosim cisla, jinak pokoj nelze vytvorit";
                return;
            }

            pokoj = new Pokoj(pocetLuzek, nosnost, mainWindow.GetPokoje().Count+1);
            lbError.Content = "";
            Close();
        }

        public Pokoj GetPokoj()
        {
            return pokoj;
        }
    }
}
